package com.spring.demo.organizationApp.dto;

import java.io.Serializable;

public class Timezone implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private String offset;

    private String description;

    public void setOffset(String offset){
        this.offset = offset;
    }
    public String getOffset(){
        return this.offset;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return this.description;
    }
}
