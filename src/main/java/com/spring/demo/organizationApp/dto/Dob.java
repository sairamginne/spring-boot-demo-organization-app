package com.spring.demo.organizationApp.dto;

import java.io.Serializable;

public class Dob implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private String date;

    private int age;

    public void setDate(String date){
        this.date = date;
    }
    public String getDate(){
        return this.date;
    }
    public void setAge(int age){
        this.age = age;
    }
    public int getAge(){
        return this.age;
    }
}
