package com.spring.demo.organizationApp.dto;

import java.io.Serializable;

public class Id implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private String name;

    private String value;

    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setValue(String value){
        this.value = value;
    }
    public String getValue(){
        return this.value;
    }
}
