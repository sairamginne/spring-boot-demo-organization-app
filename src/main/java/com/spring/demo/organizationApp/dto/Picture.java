package com.spring.demo.organizationApp.dto;

import java.io.Serializable;

public class Picture implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private String large;

    private String medium;

    private String thumbnail;

    public void setLarge(String large){
        this.large = large;
    }
    public String getLarge(){
        return this.large;
    }
    public void setMedium(String medium){
        this.medium = medium;
    }
    public String getMedium(){
        return this.medium;
    }
    public void setThumbnail(String thumbnail){
        this.thumbnail = thumbnail;
    }
    public String getThumbnail(){
        return this.thumbnail;
    }
}
