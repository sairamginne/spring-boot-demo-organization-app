package com.spring.demo.organizationApp.dto;

import java.io.Serializable;

public class Coordinates implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private String latitude;

    private String longitude;

    public void setLatitude(String latitude){
        this.latitude = latitude;
    }
    public String getLatitude(){
        return this.latitude;
    }
    public void setLongitude(String longitude){
        this.longitude = longitude;
    }
    public String getLongitude(){
        return this.longitude;
    }
}
