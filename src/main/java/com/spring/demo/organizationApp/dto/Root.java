package com.spring.demo.organizationApp.dto;
import java.io.Serializable;
import java.util.List;
public class Root implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private List<Results> results;

    private Info info;

    public void setResults(List<Results> results){
        this.results = results;
    }
    public List<Results> getResults(){
        return this.results;
    }
    public void setInfo(Info info){
        this.info = info;
    }
    public Info getInfo(){
        return this.info;
    }
}
