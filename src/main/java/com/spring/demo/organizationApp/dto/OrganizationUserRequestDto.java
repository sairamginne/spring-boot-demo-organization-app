package com.spring.demo.organizationApp.dto;

public class OrganizationUserRequestDto {

	private String gender;

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "OrganizationUserRequestDto [gender=" + gender + "]";
	}
	
	
}
