package com.spring.demo.organizationApp.user.service;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mashape.unirest.http.exceptions.UnirestException;

public interface OrganizationUserService {

	List<String> fetchNameByGender(final String gender) throws JsonMappingException, JsonProcessingException, UnirestException;

}
