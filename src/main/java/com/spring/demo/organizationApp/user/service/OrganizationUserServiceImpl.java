package com.spring.demo.organizationApp.user.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.spring.demo.organizationApp.dto.Name;
import com.spring.demo.organizationApp.dto.Results;
import com.spring.demo.organizationApp.dto.Root;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class OrganizationUserServiceImpl implements OrganizationUserService {

	private final String genderApiUrl = "https://randomuser.me/api/?format=json&gender=";

	/**
	 * Fetch the list of organization users based on the gender 
	 * @return {@link List} of users 
	 */
	public List<String> fetchNameByGender(final String gender) throws JsonMappingException, JsonProcessingException, UnirestException {
		List<String> users = new ArrayList<String>();
		Unirest.setTimeouts(0, 0);
		log.info("Gender "+gender);
		HttpResponse<String> response = Unirest.get(genderApiUrl + gender).asString();
		log.info("response: body " + response.getBody());
		ObjectMapper mapper = new ObjectMapper();
		Root json = mapper.readValue(response.getBody(), Root.class);
		for (Results result : json.getResults()) {
			Name userName = result.getName();
			users.add(userName.getFirst() + " " + userName.getLast());
		}
		return users;
	}

}
