package com.spring.demo.organizationApp.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.demo.organizationApp.dto.OrganizationUserRequestDto;
import com.spring.demo.organizationApp.user.service.OrganizationUserService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/api/org/users")
@Log4j2
public class OrganizationUserController {

	@Autowired
	OrganizationUserService organizationUserService;

	@PostMapping("/gender")
	public ResponseEntity<?> getUsersbyGendar(@RequestBody OrganizationUserRequestDto userRequestDTO) {
		try {
			log.info("User request " + userRequestDTO);
			if (userRequestDTO.getGender() == null || userRequestDTO.getGender().isEmpty()) {
				return new ResponseEntity("Invalid request !! Gender can't be null" ,HttpStatus.BAD_REQUEST);
			}
			List<String> userNames = organizationUserService.fetchNameByGender(userRequestDTO.getGender());
			return new ResponseEntity(userNames, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity("Somehting is not right please try again later!!" , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
